<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = [
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new Flosch\Bundle\ProxyBundle\FloschProxyBundle(),
        ];

        if (in_array($this->getEnvironment(), ['dev', 'test'], true)) {
            $bundles[] = new Symfony\Bundle\DebugBundle\DebugBundle();
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
        }

        return $bundles;
    }

    protected function getKernelParameters()
    {
        return array_merge(parent::getKernelParameters(), [
            'kernel.var_dir'        => $this->getVarDir(),
            'kernel.web_dir'        => $this->getWebDir(),
            'kernel.vendor_dir'     => $this->getVendorDir()
        ]);
    }

    public function getRootDir()
    {
        return __DIR__;
    }

    public function getVarDir()
    {
        return dirname(__DIR__) . '/var';
    }

    public function getWebDir()
    {
        return dirname(__DIR__) . '/web';
    }

    public function getVendorDir()
    {
        return dirname(__DIR__) . '/vendor';
    }

    public function getCacheDir()
    {
        return sprintf('%s/cache/%s', $this->getVarDir(), $this->getEnvironment());
    }

    public function getLogDir()
    {
        return sprintf('%s/logs', $this->getVarDir());
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(sprintf('%s/config/config_%s.yml', $this->getRootDir(), $this->getEnvironment()));
    }
}
